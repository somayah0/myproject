<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cars;
use App\Repositories\CarRepository;
class carController extends Controller
{
    public function index(){
        $carRepository = new CarRepository();
        $cars= $carRepository->all();
        $id = request()->id;
        return view('cars',compact('cars','id'));
    }

    public function details(){
        $cars= Cars::all();
        $id = request()->id;
        return view('details',compact('cars','id')); 
    }
}
