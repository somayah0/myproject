<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rooms;
class RoomController extends Controller
{
    public function index(){
        $rooms = Rooms::all();
        
        return view('rooms',compact('rooms')); 
    }
}
