<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/rooms', 'RoomController@index');
Route::get('/rooms/cars/{id}','carController@index');
Route::get('/rooms/cars/details/{id}','carController@details');
Route::get('/toArabic', [App\Http\Controllers\NewController::class,'toArabic']);
Route::get('/toEnglish', [App\Http\Controllers\NewController::class,'toEnglish']);
