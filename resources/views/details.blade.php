@extends('layout.app')

@section('title', 'Cars')

@section('content')
@foreach ($cars as $car)
@if($car->id == $id)
<div class="card mb-3">
  <img src="{{ asset('images/' . $car->img) }}" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">{{ $car->name }}</h5>
    <p class="card-text">Model: {{ $car->model }}</p>
  </div>
</div>
@endif
@endforeach
@endsection