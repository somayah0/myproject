@extends('layout.app')

@section('title', 'Cars Rooms')

@section('content')

@foreach ($rooms as $room)

<div class="card mb-3">
  <img src="{{ asset('images/' . $room->img) }}" class="card-img-top" style="withe:0px">
  <div class="card-body">
  <a href="rooms/cars/{{ $room->id }} " style="color:#a5a58d"><h5 class="card-title">{{ $room->name }}</h5></a>
  </div>
</div>
  
@endforeach

@endsection