<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>App name - @yield("title")</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstraapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>   
    <script src="assets/vendors/jquery/jquery-3.5.1.min.js"></script>
</head>

<body>
<header style="background-color:#fff1e6 ;">
<ul class="nav justify-content-left">
  <li class="nav-item">
    <a class="nav-link active" style="color:#a5a58d" href="rooms">
    <svg width="3em" height="3em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
      <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
    </svg></a>
  </li>
  <li >
    <h5  style="color:#a5a58d; padding-top:20% ">  {{ __('messages.welcome') }} </h5>
  </li>
</ul>
<ul>
<!-- add anthor language -->
    <li class="nav justify-content-end">
          <h5> <a class="nav-link active" style="color:#a5a58d" href="/toArabic">العربية</a></5>
          <h5><a class="nav-link active" style="color:#a5a58d" href="/toEnglish">English</a></5>
    </li>
</ul>
</header>

<br>
<br>

@if($errors->any())
  <div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
      <li> {{ $error }} </li>
    @endforeach
  </ul>
  </div>
@endif

<div class="container">
    @yield("content")
</div>

<footer class="footer mt-auto py-3">
  <div class="container">
    <span class="text-muted">&copy;Cars2020</span>
  </div>
</footer>
</body>
</html>