@extends('layout.app')

@section('title', 'Cars')

@section('content')
@foreach ($cars as $car)
@if($car->room_num == $id)


<table class="table table-striped shadow" >
  
  <tbody>
    <tr>
      <th scope="row" >Image</th>
      <td><img src="{{ asset('images/' . $car->img) }}" ></td>
    </tr>
    <tr>
      <th scope="row">Car Name</th>
      <td>{{ $car->name }}</td>
    </tr>
    <tr>
      <th scope="row">Details</th>
      <td><a href="details/{{ $car->id }}" style="color:black">Display</a></td>
    </tr>
  </tbody>
  <br>
  <br>
</table>

@endif
@endforeach
@endsection